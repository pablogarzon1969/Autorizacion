﻿namespace Data
{
    using Entities;
    using Microsoft.EntityFrameworkCore;
    using System;

    /// <summary>
    /// 
    /// </summary>
    public class ContextDB : DbContext
    {
        private string _connectionString = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="options"></param>
        public ContextDB(DbContextOptions options)
            : base(options) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionString"></param>
        public ContextDB(String connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// 
        /// </summary>
        public ContextDB() { }


        /// <summary>
        /// Permite configurar la informacion que esta siendo pasada  al DB Context.
        /// </summary>
        /// <param name="options"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            if (!string.IsNullOrEmpty(_connectionString))
            {
                options.UseSqlServer(_connectionString);
            }
            base.OnConfiguring(options);
        }

        public virtual DbSet<Application> applications { get; set; }
        public virtual DbSet<Role> roles { get; set; }
        public virtual DbSet<AssignmentRole> assignmentRoles { get; set; } 
        public virtual DbSet<Permission> permission { get; set; }

    }
}