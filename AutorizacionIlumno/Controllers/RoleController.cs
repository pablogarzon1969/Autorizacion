﻿namespace AutorizacionIlumno.Controllers
{
    using Business;
    using Entities;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Options;
    using Repository;
    using System;
    using System.Collections.Generic;

    [Produces("application/json")]
    public class RoleController : Controller
    {
        BusinessRole businessRole;
        BusinessAssignmentRole businessAssignmentRole;
        private readonly IConfiguration _Iconfiguration;
        private IUnitOfWork _unitOfWork;

        public RoleController(IConfiguration iconfiguration, IOptions<Connection> connection)
        {
            this._Iconfiguration = iconfiguration;
            this._unitOfWork = new UnitOfWork(connection.Value.DefaultConnection);
            businessAssignmentRole = new BusinessAssignmentRole(this._unitOfWork);
            businessRole = new BusinessRole(this._unitOfWork);
        }

        [HttpGet]
        [Route("Roles")]
        public IEnumerable<Role> Get(Int32? idRole, Int32? applicationId)
        {
            IEnumerable<Role> result = null;
            return result = businessRole.GetRolesFilter(idRole, applicationId);
        }

        [HttpPost]
        [Route("Role")]
        public IActionResult Post([FromBody]dynamic Entrol)
        {
            IActionResult statusreturn = null;
            try
            {
                if (Entrol != null)
                {
                    var active = Entrol.active.Value;
                    String roleName = Entrol.roleName.Value;
                    var applicationId = Entrol.applicationId.Value;
                    Role role = new Role
                    {
                        active = active,
                        roleName = roleName.ToLower(),
                        applicationId = Convert.ToInt32(applicationId)
                    };

                    if (businessRole.ValidatedRoleCreated(role) <= 0)
                    {
                        businessRole.CreateRole(role);
                        statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status201Created);
                    }
                    else
                    {
                        statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status409Conflict);
                    }
                }
            }
            catch (Exception)
            {
                statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status500InternalServerError);
            }
            return statusreturn;
        }

        [HttpPut]
        [Route("Role")]
        public IActionResult Put([FromBody]Role role)
        {
            IActionResult statusreturn = null;
            try
            {
                businessRole.UpdateRole(role);
                statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK);
            }
            catch (Exception)
            {
                statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status500InternalServerError);
            }
            return statusreturn;
        }

        [HttpDelete]
        [Route("Role/{id}")]
        public IActionResult Delete(Int32 id)
        {
            IActionResult statusreturn = null;
            try
            {
                businessRole.DeleteRole(id);
                statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK);
            }
            catch (Exception)
            {
                statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status500InternalServerError);
            }
            return statusreturn;
        }
    }
}
