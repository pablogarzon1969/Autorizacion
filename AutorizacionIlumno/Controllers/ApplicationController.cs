﻿namespace AutorizacionIlumno.Controllers
{
    using Business;
    using Entities;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Options;
    using Repository;
    using System;
    using System.Collections.Generic;

    [Produces("application/json")]
    [Route("Applications")]
    public class ApplicationController : Controller
    {

        BusinessApplication businessApplication;
        private readonly IConfiguration _Iconfiguration;
        private IUnitOfWork _unitOfWork;
        public ApplicationController(IConfiguration iconfiguration, IOptions<Connection> connection)
        {
            _Iconfiguration = iconfiguration;
            this._unitOfWork = new UnitOfWork(connection.Value.DefaultConnection);
            businessApplication = new BusinessApplication(_unitOfWork);
        }

        [HttpGet]
        public IEnumerable<Application> Get()
        {
            return businessApplication.GetApplications();
        }


        // GET: api/Application
        [HttpGet("{id}", Name = "Get")]
        public IEnumerable<Application> Get(int id)
        {
            return businessApplication.GetApplicationById(id);
        }

        [HttpPost]
        public IActionResult Post([FromBody]dynamic Entapplication)
        {
            IActionResult statusreturn = null;
            try
            {
                if (Entapplication != null)
                {
                    var active = Entapplication.active.Value;
                    String applicationName = Entapplication.applicationName.Value;
                    Application application = new Application
                    {
                        active = active,
                        applicationName = applicationName.ToLower()
                    };
                    if (businessApplication.ValidatedApplicationCreated(application) <= 0)
                    {
                        businessApplication.ApplicationCreate(application);
                        statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status201Created);
                    }
                    else
                    {
                        statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status409Conflict);
                    }
                }
            }
            catch (Exception)
            {
                statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status500InternalServerError);
            }
            return statusreturn;
        }

        // PUT: api/Application/5
        [HttpPut]
        public IActionResult Put([FromBody]Application application)
        {
            IActionResult statusreturn = null;
            try
            {
                businessApplication.UpdateApplication(application);
                statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK);
            }
            catch (Exception)
            {
                statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status500InternalServerError);
            }
            return statusreturn;
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Int32 id)
        {
            IActionResult statusreturn = null;
            try
            {
                businessApplication.DeleteApplication(id);
                statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK);
            }
            catch (Exception)
            {
                statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status500InternalServerError);
            }
            return statusreturn;
        }
    }
}
