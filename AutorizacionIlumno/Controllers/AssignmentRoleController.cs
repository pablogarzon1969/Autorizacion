﻿namespace AutorizacionIlumno.Controllers
{
    using Business;
    using Entities;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Options;
    using Repository;
    using System;
    using System.Collections.Generic;

    [Produces("application/json")]
    public class AssignmentRoleController : Controller
    {
        BusinessAssignmentRole businessAssignmentRole;
        private readonly IConfiguration _Iconfiguration;
        private IUnitOfWork _unitOfWork;

        public AssignmentRoleController(IConfiguration iconfiguration, IOptions<Connection> connection)
        {
            _Iconfiguration = iconfiguration;
            this._unitOfWork = new UnitOfWork(connection.Value.DefaultConnection);
            businessAssignmentRole = new BusinessAssignmentRole(_unitOfWork);
        }


        [HttpGet]
        [Route("AssignmentRole")]
        public IEnumerable<dynamic> Get(string user = null, Int32? applicationId = null)
        {
            return businessAssignmentRole.GetUserFilter(applicationId, user);
        }

        [HttpPost]
        [Route("AssignmentRole")]
        public IActionResult Post([FromBody]dynamic EntassigmentRole)
        {
            IActionResult statusreturn = null;
            try
            {
                if (EntassigmentRole != null)
                {
                    var roleId = EntassigmentRole.roleId.Value;
                    String username = EntassigmentRole.username.Value;
                    AssignmentRole assignmentRole = new AssignmentRole
                    {
                        roleId = Convert.ToInt32(roleId),
                        user = username.ToLower()
                    };
                    if (businessAssignmentRole.ValidatedUserCreated(assignmentRole) <= 0)
                    {
                        businessAssignmentRole.CreateAssignmentRole(assignmentRole);
                        statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status201Created);
                    }
                    else
                    {
                        statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status409Conflict);
                    }
                }                  
            }
            catch (Exception)
            {
                statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status500InternalServerError);
            }
            return statusreturn;
        }

        // PUT: api/Application/5
        [HttpPut]
        [Route("AssignmentRole")]
        public IActionResult Put([FromBody]AssignmentRole assigmentRole)
        {
            IActionResult statusreturn = null;
            try
            {
                businessAssignmentRole.UpdateAssignmentRole(assigmentRole);
                statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK);
            }
            catch (Exception)
            {
                statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status500InternalServerError);
            }
            return statusreturn;

        }

        [HttpDelete("{assignmentRoleId}")]
        public IActionResult Delete(Int32 assignmentRoleId)
        {
            IActionResult statusreturn = null;
            try
            {
                businessAssignmentRole.DeleteAssignmentRole(assignmentRoleId);
                statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK);
            }
            catch(Exception)
            {
                statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status500InternalServerError);
            }
            return statusreturn;
        }
    }
}
