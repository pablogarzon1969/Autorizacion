﻿namespace AutorizacionIlumno.Controllers
{
    using Business;
    using Entities;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Options;
    using Repository;
    using System;
    using System.Collections.Generic;

    [Produces("application/json")]
    public class PermissionController : Controller
    {

        BusinessPermission businessPermission;
        private readonly IConfiguration _Iconfiguration;
        private IUnitOfWork _unitOfWork;
        public PermissionController(IConfiguration iconfiguration, IOptions<Connection> connection)
        {
            _Iconfiguration = iconfiguration;
            this._unitOfWork = new UnitOfWork(connection.Value.DefaultConnection);
            businessPermission = new BusinessPermission(this._unitOfWork);
        }

        [HttpGet]
        [Route("Permissions/{id}")]
        public IEnumerable<Permission> GetPermission(int id)
        {
            return businessPermission.GetPermissionsById(id);
        }

        [HttpGet]
        [Route("Permissions")]
        public IEnumerable<dynamic> Get(Int32? roleId = null, string user = null, Int32? applicationId = null)
        {
            return businessPermission.GetPermissionFilter(applicationId, user, roleId);
        }

        [HttpPost]
        [Route("Permissions")]
        public IActionResult Post([FromBody]dynamic EntPermission)
        {
            IActionResult statusreturn = null;
            try
            {
                if (EntPermission != null)
                {
                    var active = EntPermission.active.Value;
                    String permissionName = EntPermission.permissionName.Value;
                    var rolId = EntPermission.rolId.Value;
                    String permissionCode = EntPermission.permissionCode.Value;
                    Permission permission = new Permission
                    {
                        active = active,
                        permissionName = permissionName.ToLower(),
                        permissionCode = permissionCode.ToLower(),
                        roleId = Convert.ToInt32(rolId)
                    };

                    if (businessPermission.ValidatedPermissionCreated(permission) <= 0)
                    {
                        businessPermission.PermissioCreate(permission);
                        statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status201Created);
                    }
                    else
                    {
                        statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status409Conflict);
                    }
                }
            }
            catch (Exception)
            {
                statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status500InternalServerError);
            }
            return statusreturn;
        }

        [HttpPut]
        [Route("Permissions")]
        public IActionResult Put([FromBody]Permission permission)
        {
            IActionResult statusreturn = null;
            try
            {
                businessPermission.UpdatePermission(permission);
                statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK);
            }
            catch (Exception)
            {
                statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status500InternalServerError);
            }
            return statusreturn;
        }

        [HttpDelete]
        [Route("Permissions/{id}")]
        public IActionResult Delete(Int32 id)
        {
            IActionResult statusreturn = null;
            try
            {
                businessPermission.DeletePermission(id);
                statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK);
            }
            catch(Exception)
            {
                statusreturn = StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status500InternalServerError);
            }
            return statusreturn;
        }
    }
}
