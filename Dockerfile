FROM microsoft/aspnetcore-build:2.0 AS build
WORKDIR /opt/autorizacion
# copy csproj and restore as distinct layers
COPY . .
RUN dotnet restore
# copy everything else and build
RUN dotnet publish --output /output --configuration Release 
# build runtime image
FROM microsoft/aspnetcore:2.0
COPY --from=build /output /app
WORKDIR /app 
#Expose port 80
EXPOSE 80
ENTRYPOINT ["dotnet", "AutorizacionIlumno.dll"]