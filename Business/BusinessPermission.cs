﻿namespace Business
{
    using Entities;
    using Repository;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Permite gestionar las operaciones de crud del perfil
    /// </summary>
    public class BusinessPermission
    {

        private IUnitOfWork _unit;

        public BusinessPermission(IUnitOfWork unit)
        {
            this._unit = unit;
        }

        /// <summary>
        /// Permite obtener los permisos que hacen parte del modulo de autorizacion
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Permission> GetPermissions()
        {
            IEnumerable<Permission> permission;
            return permission = this._unit.GenericRepository<Permission>().Get().AsEnumerable();
        }

        /// <summary>
        /// Permite obtener los permisos que hacen parte del modulo de autorizacion
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Permission> GetPermissionsById(Int32 idPermission)
        {
            IEnumerable<Permission> permission;
            return permission = this._unit.GenericRepository<Permission>().Get(x => x.Id == idPermission).AsEnumerable();
        }

        /// <summary>
        /// Obtiene los permisos que son parte del rol
        /// </summary>
        /// <param name="IdRol"></param>
        /// <returns></returns>
        public IEnumerable<Permission> GetPermissionByIdRol(Int32? IdRol)
        {
            IEnumerable<Permission> permission;
            return permission = this._unit.GenericRepository<Permission>().Get(x => x.roleId == IdRol).AsEnumerable();
        }

        public IEnumerable<dynamic> GetPermissionByIdApplication(Int32? IdApplication)
        {
            var Permission = from fpermiso in this._unit.GenericRepository<Permission>().Get()
                             join frol in this._unit.GenericRepository<Role>().Get() on fpermiso.roleId equals frol.Id
                             join fAssignmentRole in this._unit.GenericRepository<AssignmentRole>().Get() on frol.Id equals fAssignmentRole.roleId
                             where frol.applicationId== IdApplication
                             select new
                             {
                                 permissionCode = fpermiso.permissionCode,
                                 active = fpermiso.active,
                                 roleId = fpermiso.roleId
                             };
            return Permission.ToList();
        }


        public IEnumerable<dynamic> GetPermissionFilter(Int32? idApplication, String user, Int32? roleId)
        {
            dynamic result = null;
            if (roleId == null && idApplication == null && user == null)
            {
                result = this.GetPermissions();
            }

            else if (roleId != null && idApplication != null && user != null)
            {
                result = this.GetPermissionByUser(idApplication, user.ToLower(), roleId);
            }

            else if (roleId != null && idApplication == null &&  user != null)
            {
                result = this.GetPermissionByUserAndRol(user.ToLower(), roleId);
            }


            else if (roleId != null && idApplication == null && user == null)
            {
                result = this.GetPermissionByIdRol(roleId);
            }

            else if (roleId == null && idApplication != null && user == null)
            {
                result = this.GetPermissionByIdApplication(idApplication);
            }

            else if (roleId == null && idApplication != null &&  user != null)
            {
                result = this.GetPermissionByApplicationAndUser(user.ToLower(), idApplication);
            }

            else if (roleId != null && idApplication != null && user == null)
            {
                result = this.GetPermissionByApplicationAndRol(idApplication, roleId);
            }          
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public short ValidatedPermissionCreated(Permission permission)
        {
            short msgValidation = 0;
            var Assignmentrole = (from fpermission in _unit.GenericRepository<Permission>().Get()
                                  where fpermission.roleId == permission.roleId && fpermission.permissionCode == permission.permissionCode
                                  select fpermission).Count();

            if (Assignmentrole > 0)
            {
                msgValidation = 1;
            }

            return msgValidation;
        }

        /// <summary>
        /// Permite crear el permiso que tendran el usuario
        /// </summary>
        /// <param name="perfil"></param>
        /// <returns></returns>
        public Permission PermissioCreate(Permission permiso)
        {
            this._unit.GenericRepository<Permission>().Insert(permiso);
            this._unit.Save();
            return permiso;
        }



        public IEnumerable<dynamic> GetPermissionByUserAndRol(String user, Int32? roleId)
        {
            var Permission = from fpermiso in this._unit.GenericRepository<Permission>().Get()
                             join frol in this._unit.GenericRepository<Role>().Get() on fpermiso.roleId equals frol.Id
                             join fAssignmentRole in this._unit.GenericRepository<AssignmentRole>().Get() on frol.Id equals fAssignmentRole.roleId
                             where fpermiso.roleId == roleId && fAssignmentRole.user == user
                             select new
                             {
                                 permissionCode = fpermiso.permissionCode,
                                 active = fpermiso.active,
                                 roleId = fpermiso.roleId,
                                 permissionName = fpermiso.permissionName,
                                 roleName = frol.roleName
                             };
            return Permission.ToList();
        }



        public IEnumerable<dynamic> GetPermissionByUser(Int32? idApplication, String user, Int32? roleId)
        {
            var Permission = from fpermiso in this._unit.GenericRepository<Permission>().Get()
                             join frol in this._unit.GenericRepository<Role>().Get() on fpermiso.roleId equals frol.Id
                             join fAssignmentRole in this._unit.GenericRepository<AssignmentRole>().Get() on frol.Id equals fAssignmentRole.roleId
                             where fpermiso.roleId == roleId && fAssignmentRole.user == user && frol.applicationId == idApplication
                             select new
                             {
                                 permissionCode = fpermiso.permissionCode,
                                 active = fpermiso.active,
                                 roleId = fpermiso.roleId,
                                 permissionName = fpermiso.permissionName,
                                 roleName = frol.roleName
                             };
            return Permission.ToList();
        }

        public IEnumerable<dynamic> GetPermissionByApplicationAndUser(String user, Int32? idApplication)
        {
            var Permission = from fpermiso in this._unit.GenericRepository<Permission>().Get()
                             join frol in this._unit.GenericRepository<Role>().Get() on fpermiso.roleId equals frol.Id
                             join fAssignmentRole in this._unit.GenericRepository<AssignmentRole>().Get() on frol.Id equals fAssignmentRole.roleId
                             where frol.applicationId == idApplication && fAssignmentRole.user == user
                             select new
                             {
                                 permissionCode = fpermiso.permissionCode,
                                 active = fpermiso.active,
                                 roleId = fpermiso.roleId,
                                 permissionName = fpermiso.permissionName,
                                 roleName = frol.roleName
                             };
            return Permission.ToList();
        }

        public IEnumerable<dynamic> GetPermissionByApplicationAndRol(Int32? idApplication, Int32? roleId)
        {
            var Permission = from fpermiso in this._unit.GenericRepository<Permission>().Get()
                             join frol in this._unit.GenericRepository<Role>().Get() on fpermiso.roleId equals frol.Id
                             join fAssignmentRole in this._unit.GenericRepository<AssignmentRole>().Get() on frol.Id equals fAssignmentRole.roleId
                             where frol.applicationId == idApplication && fpermiso.roleId == roleId
                             select new
                             {
                                 permissionCode = fpermiso.permissionCode,
                                 active = fpermiso.active,
                                 roleId = fpermiso.roleId,
                                 permissionName = fpermiso.permissionName,
                                 roleName = frol.roleName
                             };
            return Permission.ToList().Distinct();
        }


        /// <summary>
        /// Permite eliminar el perfil que utilizara la autorizacion
        /// </summary>
        /// <param name="id"></param>
        public void DeletePermission(Int32 id)
        {
            var rol = this._unit.GenericRepository<Permission>().GetByID(id);
            if (rol != null)
            {
                this._unit.GenericRepository<Permission>().Delete(rol);
                this._unit.Save();
            }
        }
        /// <summary>
        /// Permite actualizar la informacion correspondiente al permiso creado para el modulo de autorizacion
        /// </summary>
        /// <param name="perfil"></param>
        /// <returns></returns>
        public String UpdatePermission(Permission permission)
        {
            String msg;
            var permisoconsulta = this._unit.GenericRepository<Permission>().GetByID(permission.Id);

            if (permisoconsulta == null)
            {
                msg = "Permiso no encontrado";
            }
            else
            {
                permisoconsulta.permissionName = permission.permissionName.ToLower();
                permisoconsulta.permissionCode = permission.permissionCode.ToLower();
                permisoconsulta.active = permission.active;

                this._unit.GenericRepository<Permission>().Update(permisoconsulta);
                this._unit.Save();
                msg = "Dato Guardado satifactoriamente";
            }
            return msg;
        }
    }
}
