﻿namespace Business
{
    using Entities;
    using Repository;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// 
    /// </summary>
    public class BusinessAssignmentRole
    {
        private IUnitOfWork _unit;

        public BusinessAssignmentRole(IUnitOfWork unit)
        {
            this._unit = unit;
        }

        public IEnumerable<dynamic> GetUserFilter(Int32? idApplication, String user)
        {
            dynamic result = null;
            if (idApplication == null && user == null)
            {
                result = this.GetUsers();
            }

            else if (idApplication != null && user != null)
            {
                result = this.GetUsersByApplicationAndUser(idApplication, user.ToLower());
            }

            else if (idApplication != null && user == null)
            {
                result = GetUsersByIdApplication(idApplication);
            }
            return result;
        }


        public IEnumerable<dynamic> GetUsers()
        {
            var Assignmentrole = from fassignmentrole in _unit.GenericRepository<AssignmentRole>().Get()
                                 join frole in this._unit.GenericRepository<Role>().Get() on fassignmentrole.roleId equals frole.Id
                                 join fapplication in this._unit.GenericRepository<Application>().Get() on frole.applicationId equals fapplication.Id
                                 select new
                                 {
                                     assignmentRoleId = fassignmentrole.Id,
                                     user = fassignmentrole.user,
                                     roleId = frole.Id,
                                     role = frole.roleName,
                                     roleactive = frole.active,
                                     applicationId = fapplication.Id,
                                     application = fapplication.applicationName

                                 };
            return Assignmentrole.ToList();
        }

        public IEnumerable<dynamic> GetUsersByApplicationAndUser(Int32? IdApplication, String user)
        {
            var Assignmentrole = from fassignmentrole in _unit.GenericRepository<AssignmentRole>().Get()
                                 join frole in _unit.GenericRepository<Role>().Get() on fassignmentrole.roleId equals frole.Id
                                 where frole.applicationId == IdApplication && fassignmentrole.user == user
                                 select new
                                 {
                                     assignmentRoleId = fassignmentrole.Id,
                                     user = fassignmentrole.user,
                                     roleId = frole.Id,
                                     role = frole.roleName,
                                     roleactive = frole.active
                                 };
            return Assignmentrole.ToList();
        }


        public IEnumerable<dynamic> GetUsersByIdApplication(Int32? IdApplication)
        {
            var Assignmentrole = from fassignmentrole in _unit.GenericRepository<AssignmentRole>().Get()
                                 join frole in _unit.GenericRepository<Role>().Get() on fassignmentrole.roleId equals frole.Id

                                 where frole.applicationId == IdApplication
                                 select new
                                 {
                                     assignmentRoleId = fassignmentrole.Id,
                                     user = fassignmentrole.user,
                                     roleId = frole.Id,
                                     role = frole.roleName,
                                     roleactive = frole.active
                                 };
            return Assignmentrole.ToList();
        }

        /// <summary>
        /// Permite crear la asignacion del rol al usuario
        /// </summary>
        /// <param name="rol"></param>
        /// <returns></returns>
        public AssignmentRole CreateAssignmentRole(AssignmentRole assignmentRole)
        {
            this._unit.GenericRepository<AssignmentRole>().Insert(assignmentRole);
            this._unit.Save();
            return assignmentRole;
        }

        /// <summary>
        /// Allow validated if a user is register again
        /// </summary>
        /// <param name="assignmentRole"></param>
        /// <returns></returns>
        public short ValidatedUserCreated(AssignmentRole assignmentRole)
        {
            short msgValidation = 0;

            var Assignmentuser = (from fassignmentrole in _unit.GenericRepository<AssignmentRole>().Get()
                                 join frole in _unit.GenericRepository<Role>().Get() on fassignmentrole.roleId equals frole.Id
                                 where fassignmentrole.user == assignmentRole.user && fassignmentrole.roleId == assignmentRole.roleId
                                 select fassignmentrole).Count();

            if (Assignmentuser > 0)
            {
                msgValidation = 1;
            }

            return msgValidation;
        }


        /// <summary>
        /// Permite actualizar la informacion correspondiente a la asignacion del rol al usuario
        /// </summary>
        /// <param name="aplicacion"></param>
        /// <returns></returns>
        public String UpdateAssignmentRole(AssignmentRole assignmentRole)
        {
            String msg;
            var searchassignmentRole = this._unit.GenericRepository<AssignmentRole>().GetByID(assignmentRole.Id);

            if (searchassignmentRole == null)
            {
                msg = "Aplicacion no encontrada";
            }
            else
            {
                searchassignmentRole.user = assignmentRole.user.ToLower();
                searchassignmentRole.roleId = assignmentRole.roleId;

                this._unit.GenericRepository<AssignmentRole>().Update(searchassignmentRole);
                this._unit.Save();
                msg = "Dato Guardado satifactoriamente";
            }
            return msg;
        }

        /// <summary>
        /// Permite eliminar el uusario con su respectiva asignacion de  rol
        /// </summary>
        /// <param name="id"></param>
        public void DeleteAssignmentRole(Int32 assignmentRoleId)
        {
            var assignmentRole = this._unit.GenericRepository<AssignmentRole>().GetByID(assignmentRoleId);
            if (assignmentRole != null)
            {
                this._unit.GenericRepository<AssignmentRole>().Delete(assignmentRole);
                this._unit.Save();
            }
        }
    }
}
