﻿namespace Business
{
    using Entities;
    using Repository;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Permite gestionar las operaciones de crud en la aplicacion
    /// </summary>
    public class BusinessApplication
    {
        private IUnitOfWork _unit;

        public BusinessApplication(IUnitOfWork unit)
        {
            this._unit = unit;
        }

        /// <summary>
        /// Permite obtener las aplicaciones que hacen parte del modulo de autorizacion
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Application> GetApplications()
        {
            IEnumerable<Application> application;
            return application = this._unit.GenericRepository<Application>().Get().AsEnumerable();
        }


        public IEnumerable<Application> GetApplicationById(Int32 IdApplication)
        {
            IEnumerable<Application> application;
            return application = this._unit.GenericRepository<Application>().Get(x => x.Id == IdApplication).AsEnumerable();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="application"></param>
        /// <returns></returns>
        public short ValidatedApplicationCreated(Application application)
        {
            short msgValidation = 0;
            var Assignmentrole = (from fapplication in _unit.GenericRepository<Application>().Get()
                                  where fapplication.applicationName == application.applicationName
                                  select fapplication).Count();

            if (Assignmentrole > 0)
            {
                msgValidation = 1;
            }

            return msgValidation;
        }


        /// <summary>
        /// Permite crear la aplicacion que utilizara la autorizacion
        /// </summary>
        /// <param name="aplicacion"></param>
        /// <returns></returns>
        public Application ApplicationCreate(Application aplicacion)
        {
            this._unit.GenericRepository<Application>().Insert(aplicacion);
            this._unit.Save();
            return aplicacion;
        }

        /// <summary>
        /// Permite eliminar la aplicacion que utilizara la autorizacion
        /// </summary>
        /// <param name="id"></param>
        public void DeleteApplication(Int32 id)
        {
            var application = this._unit.GenericRepository<Application>().GetByID(id);
            if (application != null)
            {
                this._unit.GenericRepository<Application>().Delete(application);
                this._unit.Save();
            }
        }

        /// <summary>
        /// Permite actualizar la informacion correspondiente a la aplicacion creada para el modulo de autorizacion
        /// </summary>
        /// <param name="aplicacion"></param>
        /// <returns></returns>
        public String UpdateApplication(Application application)
        {
            String msg;
            var searchApplication = this._unit.GenericRepository<Application>().GetByID(application.Id);

            if (searchApplication == null)
            {
                msg = "Aplicacion no encontrada";
            }
            else
            {
                searchApplication.applicationName = application.applicationName.ToLower();
                searchApplication.active = application.active;

                this._unit.GenericRepository<Application>().Update(searchApplication);
                this._unit.Save();
                msg = "Dato Guardado satifactoriamente";
            }
            return msg;
        }
    }
}
