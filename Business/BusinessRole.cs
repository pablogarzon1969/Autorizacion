﻿namespace Business
{
    using Entities;
    using Repository;
    using System;
    using System.Collections.Generic;
    using System.Linq;


    /// <summary>
    /// Permite gestionar las operaciones de crud en el rol
    /// </summary>
    public class BusinessRole
    {
        private IUnitOfWork _unit;

        public BusinessRole(IUnitOfWork unit)
        {
            this._unit = unit;
        }

        /// <summary>
        /// Permite obtener los roles que hacen parte del modulo de autorizacion
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Role> GetRolesByApplication(Int32? IdApplication)
        {
            IEnumerable<Role> role;
            return role = this._unit.GenericRepository<Role>().Get(x => x.application.Id == IdApplication);
        }

        public IEnumerable<Role> GetRoles()
        {
            IEnumerable<Role> role;
            return role = this._unit.GenericRepository<Role>().Get().AsEnumerable();
        }

        public IEnumerable<Role> GetRolesFilter(Int32? filterIdRole, Int32? filterApplicationId)
        {
            IEnumerable<Role> result = null;
            if (filterIdRole == null && filterApplicationId == null)
            {
                result = GetRoles();
            }
            else if (filterApplicationId != null && filterIdRole == null)
            {
                result = GetRolesByApplication(filterApplicationId);
            }
            else if (filterApplicationId == null && filterIdRole != null)
            {
                result = GetRolesById(filterIdRole);
            }
            else if (filterApplicationId != null && filterIdRole != null)
            {
                result = GetRolesByIdRoleApplication(filterIdRole, filterApplicationId);
            }
            return result;
        }


        public IEnumerable<Role> GetRolesById(Int32? Id)
        {
            IEnumerable<Role> role;
            return role = this._unit.GenericRepository<Role>().Get(x => x.Id == Id);
        }

        public IEnumerable<Role> GetRolesByIdRoleApplication(Int32? IdRole, Int32? idApplication)
        {
            IEnumerable<Role> role;
            return role = this._unit.GenericRepository<Role>().Get(x => x.Id == IdRole && x.applicationId == idApplication);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public short ValidatedRoleCreated(Role role)
        {
            short msgValidation = 0;
            var Assignmentrole = (from frole in _unit.GenericRepository<Role>().Get()
                                  where frole.roleName == role.roleName && frole.applicationId==role.applicationId
                                  select frole).Count();

            if (Assignmentrole > 0)
            {
                msgValidation = 1;
            }

            return msgValidation;
        }

        /// <summary>
        /// Permite crear el rol que tendran el usuario
        /// </summary>
        /// <param name="rol"></param>
        /// <returns></returns>
        public Role CreateRole(Role role)
        {
            this._unit.GenericRepository<Role>().Insert(role);
            this._unit.Save();
            return role;
        }

        /// <summary>
        /// Permite eliminar el rol que utilizara la autorizacion
        /// </summary>
        /// <param name="id"></param>
        public void DeleteRole(Int32 id)
        {
            var rol = this._unit.GenericRepository<Role>().GetByID(id);
            if (rol != null)
            {
                this._unit.GenericRepository<Role>().Delete(rol);
                this._unit.Save();
            }
        }
        /// <summary>
        /// Permite actualizar la informacion correspondiente al rol creado para el modulo de autorizacion
        /// </summary>
        /// <param name="rol"></param>
        /// <returns></returns>
        public String UpdateRole(Role role)
        {
            String msg;
            var searchRole = this._unit.GenericRepository<Role>().GetByID(role.Id);

            if (searchRole == null)
            {
                msg = "Rol no encontrada";
            }
            else
            {
                searchRole.roleName = role.roleName.ToLower();
                searchRole.active = role.active;

                this._unit.GenericRepository<Role>().Update(searchRole);
                this._unit.Save();
                msg = "Dato Guardado satifactoriamente";
            }
            return msg;
        }
    }
}
