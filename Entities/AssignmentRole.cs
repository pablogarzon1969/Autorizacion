﻿namespace Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class AssignmentRole
    {
        [Key]
        public Int32 Id
        {
            get; set;
        }

        public String  user
        {
            get; set;
        }

        [Required]
        public int roleId
        {
            get; set;
        }

        [ForeignKey("roleId")]
        public virtual Role role
        {
            get; set;
        }
    }
}
