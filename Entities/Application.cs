﻿namespace Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class Application
    {
        [Key]
        public Int32 Id
        {
            get; set;
        }
        [Required]
        [MaxLength(100)]
        public String applicationName
        {
            get; set;
        }
        [Required]
        public Boolean active
        {
            get; set;
        }
    }
}
