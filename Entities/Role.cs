﻿namespace Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Role
    {
        [Key]
        public Int32 Id
        {
            get; set;
        }

        public int applicationId
        {
            get; set;
        }

        [Required]
        [MaxLength(100)]
        public String roleName
        {
            get; set;
        }

        [Required]
        public Boolean active
        {
            get; set;
        }

        [ForeignKey("applicationId")]
        public virtual Application application
        {
            get; set;
        }
    }
}
