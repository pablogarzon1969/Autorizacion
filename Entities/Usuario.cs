﻿namespace Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Usuario
    {
        [Key]
        public Int64 Id
        {
            get; set;
        }

        public int aplicacionId
        {
            get; set;
        }

        public int rolId
        {
            get; set;
        }

        [MaxLength(100)]
        public String UsuarioAutorizado
        {
            get; set;
        }
        
        [ForeignKey("rolId")]
        public virtual Role rol
        {
            get; set;
        }

        [ForeignKey("aplicacionId")]
        public virtual Application aplicaciones
        {
            get; set;
        }

       
    }
}

