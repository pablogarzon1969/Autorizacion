﻿namespace Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Permission
    {
        [Key]
        public Int32 Id
        {
            get; set;
        }

        [Required]
        [MaxLength(100)]
        public String permissionName
        {
            get; set;
        }

        [Required]
        [MaxLength(100)]
        public String permissionCode
        {
            get; set;
        }

        [Required]
        public Boolean active
        {
            get; set;
        }

        public int roleId
        {
            get; set;
        }

        [ForeignKey("roleId")]
        public virtual Role role
        {
            get; set;
        }

    }
}
