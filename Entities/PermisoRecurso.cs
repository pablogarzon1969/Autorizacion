﻿namespace Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class PermisoRecurso
    {
        [Key]
        public Int32 Id
        {
            get; set;
        }

        public String NameController
        {
            get; set;
        }

        public String Action
        {
            get; set;
        }

        //public Boolean Consultar
        //{
        //    get; set;
        //}
        //public Boolean Crear
        //{
        //    get; set;
        //}

        //public Boolean Editar
        //{
        //    get; set;
        //}

        //public Boolean Eliminar
        //{
        //    get; set;
        //}

        public Boolean Active
        {
            get; set;
        }

        public int permisoId
        {
            get; set;
        }

        [ForeignKey("permisoId")]
        public virtual Permission permiso
        {
            get; set;
        }

    }
}
