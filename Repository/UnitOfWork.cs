﻿namespace Repository
{
    using Data;
    using Microsoft.EntityFrameworkCore.Storage;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        readonly ContextDB Context;
        private IDbContextTransaction _transaction;
        private Dictionary<Type, object> repositories = new Dictionary<Type, object>();

        /// <summary>
        /// Permite que la clase se cree usando la cadena de conexión suministrada
        /// </summary>
        /// <param name="connectionString">La cadena de conexión utilizada para conectarse a la base de datos.</param>
        public UnitOfWork(string connectionString)
        {
            this.Context = new ContextDB(connectionString);
        }

        /// <summary>
        /// Permite crear clases utilizando DB Context inyectado por aplicación
        /// </summary>
        /// <param name="context"></param>
        public UnitOfWork(ContextDB context)
        {
            this.Context = context;
            this._transaction = this.Context.Database.BeginTransaction();
        }

        /// <summary>
        /// Permite trabajar con la entidad que realizara las operaciones en la unidad de trabajo
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IGenericRepository<T> GenericRepository<T>() where T : class
        {
            if (repositories.Keys.Contains(typeof(T)) == true)
            {
                return repositories[typeof(T)] as IGenericRepository<T>;
            }

            IGenericRepository<T> repo = new GenericRepository<T>(Context);
            repositories.Add(typeof(T), repo);
            return repo;
        }      

        /// <summary>
        /// Metodo que permite realizar la operacion de commit
        /// </summary>
        public void Save()
        {
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Metodo que permite manejar las operaciones de transaccion
        /// </summary>
        public void SaveTransaction()
        {
            try
            {
                this._transaction = this.Context.Database.BeginTransaction();
                if (this._transaction != null)
                {
                    this.Context.SaveChanges();
                    this._transaction.Commit();
                    this._transaction = null;
                }
            }
            catch (Exception)
            {
                this._transaction.Rollback();
            }
        }


        private bool disposed = false;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (_transaction != null)
            {
                _transaction.Rollback();
                _transaction = null;
            }

            if (!this.disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
