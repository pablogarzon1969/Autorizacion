﻿namespace Repository
{
    /// <summary>
    /// 
    /// </summary>
    public interface IUnitOfWork
    {
        IGenericRepository<TEntity> GenericRepository<TEntity>() where TEntity : class;
        void Save();
        void SaveTransaction();
        void Dispose();
    }
}
